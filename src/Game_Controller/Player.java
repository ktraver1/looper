package Game_Controller;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Rectangle;

public class Player implements GameObject {
	private int xcoor, ycoor, width, height,speed,gravity,accelerateX,accelerateY;
	private int collisionBuffer;
	private ObjectState state;
	public Player(int xcoor, int ycoor, int tilesize) {
		this.xcoor = xcoor;
		this.ycoor = ycoor;
		this.width = tilesize;
		this.height = tilesize;
		this.speed = 1;
		this.state = ObjectState.RUNNING;
		this.gravity = 1;
		this.collisionBuffer = 10;
		this.accelerateX = 0;
		this.accelerateY = this.gravity;
	}
	public void tick() {
		//apply gravity
		if(this.accelerateY < this.gravity) {
			this.accelerateY += this.gravity;
		}
		if(this.accelerateY > this.gravity) {
			this.accelerateY -= this.gravity;
		}
		if(this.accelerateX < 0) {
			this.accelerateX += this.gravity;
		}
		if(this.accelerateX > 0) {
			this.accelerateX -= this.gravity;
		}
		move(this.accelerateX,this.accelerateY);
	}
	public void draw(Graphics g) {
		g.setColor(Color.YELLOW);
		g.fillRect(xcoor, ycoor, width, height);
		g.setColor(Color.BLUE);
		g.fillRect(this.xcoor,this.ycoor+this.height,this.width,this.collisionBuffer);
		g.setColor(Color.RED);
		g.fillRect(this.xcoor,this.ycoor-this.collisionBuffer,this.width,this.collisionBuffer);
		g.setColor(Color.WHITE);
		g.fillRect(this.xcoor-this.collisionBuffer,this.ycoor,this.collisionBuffer,this.height);
		g.setColor(Color.ORANGE);
		g.fillRect(this.xcoor+this.width,this.ycoor,this.collisionBuffer,this.height);
	}
	public void accelerate(int accelerateX,int accelerateY) {
		this.accelerateX += accelerateX;
		this.accelerateY += accelerateY;
		
	}
	public void move(int xDelta, int yDelta) {
		this.xcoor += xDelta;
		this.ycoor += yDelta;
	}
	public void moveRight() {
		move(this.speed,0);
	}
	public void moveLeft() {
		move(-this.speed,0);
	}
	public void moveUp() {
		move(0,-this.speed);
	}
	public void moveDown() {
		move(0,this.speed);
	}
	//Jump away from wall with some force to make this work
	//seperate method wall jump?
	public void jump() {
		//serpeate code if needed and look into design patterns for this
		if (this.state != ObjectState.JUMPING ) {
			this.state  = ObjectState.JUMPING;
			int xForce = 0;
			accelerate(xForce,-this.speed*10);
		}
	}
	@Override
	public int getHeight() {
		return this.height;
	}
	@Override
	public int getWidth() {
		return this.width;
	}
	@Override
	public int getXcoor() {
		return xcoor;
	}
	@Override
	public int getYcoor() {
		return ycoor;
	}
	@Override
	public void setXcoor(int xcoor) {
		this.xcoor = xcoor;
	}
	@Override
	public void setYcoor(int ycoor) {
		this.ycoor = ycoor;
	}
	@Override
	public Rectangle getBounds() {
		return new Rectangle(this.xcoor, this.ycoor, this.width, this.height);
	}
	/**
	 * Check if the object is touching player at all
	 * @param object
	 * @return boolean true of touching
	 */
	public boolean checkCollision(GameObject object) {
		/*
		 * Input: other object check if player is cooliding with
		 * Output: boolean of if objects rectangle are cooliding
		 */	
		Rectangle objectBounds = object.getBounds();
		Rectangle playerBounds = this.getBounds();
		return playerBounds.intersects(objectBounds);
	}
	/**
	 * Check if the object ontop of player and they are touching
	 * @param object
	 * @return boolean true of touching
	 */
	public boolean checkTopCollision(GameObject object) {
		Rectangle topBounds = new Rectangle(this.xcoor,this.ycoor-this.collisionBuffer,this.width,this.collisionBuffer);
		Rectangle objectBounds = object.getBounds();
		return topBounds.intersects(objectBounds);
	}
	/**
	 * Check if the object is bellow player and they are touching
	 * @param object
	 * @return boolean true of touching
	 */
	public boolean checkBottomCollision(GameObject object) {
		Rectangle bottomBounds = new Rectangle(this.xcoor,this.ycoor+this.height,this.width,this.collisionBuffer);
		Rectangle objectBounds = object.getBounds();
		return bottomBounds.intersects(objectBounds);

	}
	/**
	 * Check if the object is to the left of player and they are touching
	 * @param object
	 * @return boolean true of touching
	 */
	public boolean checkLeftCollision(GameObject object) {
		Rectangle leftBounds = new Rectangle(this.xcoor-this.collisionBuffer,this.ycoor,this.collisionBuffer,this.height);
		Rectangle objectBounds = object.getBounds();
		return leftBounds.intersects(objectBounds);
	}
	/**
	 * Check if the object is to the right of player and they are touching
	 * @param object
	 * @return boolean true of touching
	 */
	public boolean checkRightCollision(GameObject object) {
		Rectangle rightBounds = new Rectangle(this.xcoor+this.width,this.ycoor,this.collisionBuffer,this.height);
		Rectangle objectBounds = object.getBounds();
		return rightBounds.intersects(objectBounds);
	}
	/**
	 * Player collided with solid object below player and needs to stop moving down
	 * @param object
	 */
	public void hitBottom(GameObject object) {
		this.state = ObjectState.RUNNING;
		this.setYcoor(object.getYcoor()-this.getHeight()-this.collisionBuffer);
	}
	/**
	 * Player collided with solid object above player and needs to stop moving up
	 * @param object
	 */
	public void hitTop(GameObject object) {
		this.setYcoor(object.getYcoor()+this.getHeight()+this.collisionBuffer);
	}
	/**
	 * Player collided with solid object to the left and needs to stop moving left
	 * @param object
	 */
	public void hitLeft(GameObject object) {
		this.state = ObjectState.RUNNING;
		this.setXcoor(object.getXcoor()+object.getWidth()+this.collisionBuffer);
	}
	/**
	 * Player collided with solid object to the right and needs to stop moving right
	 * @param object
	 */
	public void hitRight(GameObject object) {
		this.state = ObjectState.RUNNING;
		this.setXcoor(object.getXcoor()-this.getWidth()-this.collisionBuffer);
	}

}

