package Game_Controller;
/**
 * current state of game object
 * JUMPING,RUNNING,DEAD,SHOOTING
 * @author ktravers
 *
 */
public enum ObjectState {
	JUMPING, RUNNING, DEAD;
}
