package Game_Controller;

import java.awt.Rectangle;
/**
 * interface for all game objects, methods and variables all game objects need
 * @author ktravers
 *
 */
public interface GameObject {
	public int xcoor = 0;
	public int ycoor = 0;
	public int getXcoor();
	public int getYcoor();
	public int getHeight();
	public int getWidth();
	public void setXcoor(int xcoor);
	public void setYcoor(int ycoor);
	public Rectangle getBounds();
	
}
