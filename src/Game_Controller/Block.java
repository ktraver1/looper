package Game_Controller;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Rectangle;
/**
 * Block class that will be used for solid object in game player can interact with
 * @author ktravers
 *
 */
public class Block implements GameObject{
	private int xcoor, ycoor,  width, height;
	public Block(int xcoor, int ycoor, int width, int height) {
		this.xcoor = xcoor;
		this.ycoor = ycoor;
		this.width = width;
		this.height = height;
	}
	
	public void draw(Graphics g) {
		g.setColor(Color.GREEN);
		g.fillRect(xcoor, ycoor, width, height);
	}
	@Override
	public int getHeight() {
		return this.height;
	}
	@Override
	public int getWidth() {
		return this.width;
	}
	@Override
	public int getXcoor() {
		return this.xcoor;
	}

	@Override
	public int getYcoor() {
		return this.ycoor;
	}

	@Override
	public void setXcoor(int xcoor) {
		this.xcoor = xcoor;
	}

	@Override
	public void setYcoor(int ycoor) {
		this.xcoor = ycoor;
	}

	@Override
	public Rectangle getBounds() {
		return new Rectangle(this.xcoor, this.ycoor, this.width, this.height);
	}
}
