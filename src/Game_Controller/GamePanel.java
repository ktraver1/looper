package Game_Controller;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.util.ArrayList;

import javax.swing.JPanel;
/**
 * 
 * @author Kevin Travers
 *
 */
public class GamePanel extends JPanel implements Runnable, KeyListener{
	private static final long serialVersionID = 1L;
	
	public static final int WIDTH = 500;
	public static final int HEIGHT = 500;
	
	private Thread thread;
	
	private boolean running;
	
	private Player player;

	private int size = 20;
	
	private int ticks = 0;
	
	private boolean right = true;
	private boolean left = false;
	private boolean up = false;
	private boolean down = false;
	private boolean jump = false;

	private boolean boost = false;
	
	private ArrayList<Block> blockList;
	
	private Block ground;
	
	public GamePanel() {
		setFocusable(true);
		addKeyListener(this);
		setPreferredSize(new Dimension(WIDTH,HEIGHT));
		player = new Player(100, 300, size);
		blockList = new ArrayList<Block>();
		blockList.add(new Block(0,400,50,30));
		blockList.add(new Block(50,400,50,30));
		blockList.add(new Block(50,370,50,30));
		blockList.add(new Block(100,400,50,30));
		blockList.add(new Block(150,400,50,30));
		blockList.add(new Block(200,400,50,30));
		blockList.add(new Block(250,370,50,30));

		//ground = new Block(0,400,400,30);
		start();
	}
	public void start() {
		running= true;
		thread = new Thread(this);
		thread.start();
	}
	public void stop() {
		running = false;
		try {
			thread.join();
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	public void tick() {
		ticks++;
		if(ticks > 250000) {
			if(right) {
				player.moveRight();
			}
			if(left) {
				player.moveLeft();
			}
			if(up) {
				player.moveUp();
			}
			if(down) {
				player.moveDown();
			}
			if(jump) {
				player.jump();
			}
			ticks = 0;
			player.tick();
			for(int i = 0; i < blockList.size();i++) {
				Block block = blockList.get(i);
				if(this.player.checkBottomCollision(block)) {
					this.player.hitBottom(block);
				}
				if(this.player.checkTopCollision(block)) {
					this.player.hitTop(block);
				}
				if(this.player.checkRightCollision(block)) {
					this.player.hitRight(block);
				}
				if(this.player.checkLeftCollision(block)) {
					this.player.hitLeft(block);
				}	
			}

		}
	}
	public void paint(Graphics g) {
		g.clearRect(0, 0, WIDTH, HEIGHT);
		g.setColor(Color.BLACK);
		g.fillRect(0, 0, WIDTH, HEIGHT);
		for(int i = 0; i < WIDTH/10; i++){
			g.drawLine(i*10, 0, i*10, HEIGHT);
		}
		for(int i = 0; i < HEIGHT/10; i++){
			g.drawLine(0, i*10, HEIGHT, i*10);
		}
		player.draw(g);
		for(int i = 0; i < blockList.size();i++) {
			blockList.get(i).draw(g);
		}
	}
	@Override
	public void run() {
		while(running) {
			tick();
			repaint();
		}
	}
	@Override
	public void keyTyped(KeyEvent e) {
		// TODO Auto-generated method stub
		
	}
	@Override
	public void keyPressed(KeyEvent e) {
        int key = e.getKeyCode();
        if(key == KeyEvent.VK_D) {
            right = true;
            left = false;
        }
        if(key == KeyEvent.VK_A) {
            right = false;
            left = true;
        }
        if(key == KeyEvent.VK_W) {
            up = true;
        }
        if(key == KeyEvent.VK_S) {
            down = true;
        }
        if(key == KeyEvent.VK_SPACE) {
            jump = true;
        }

	}
	@Override
	public void keyReleased(KeyEvent e) {
        int key = e.getKeyCode();
        if(key == KeyEvent.VK_D) {
            right = false;
        }
        if(key == KeyEvent.VK_A) {
            left = false;
        }
        if(key == KeyEvent.VK_W) {
            up = false;
        }
		if(key == KeyEvent.VK_SPACE) {
        	jump = false;
        }
	}
}
